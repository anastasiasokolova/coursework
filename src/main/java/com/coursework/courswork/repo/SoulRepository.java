package com.coursework.courswork.repo;

import com.coursework.courswork.entities.Soul;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.jdbc.core.JdbcTemplate;

public interface SoulRepository extends CrudRepository<Soul, Integer> {
}
