package com.coursework.courswork.repo;

import com.coursework.courswork.entities.Demon_card;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface DemonCardRepository extends CrudRepository<Demon_card, Integer> {
}
