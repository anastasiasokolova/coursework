package com.coursework.courswork.repo;

import com.coursework.courswork.entities.Worker;
import com.coursework.courswork.entities.World;
import org.springframework.data.repository.CrudRepository;

public interface WorldRepository extends CrudRepository<World, Integer> {
}
