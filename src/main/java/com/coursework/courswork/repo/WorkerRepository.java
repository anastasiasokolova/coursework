package com.coursework.courswork.repo;

import com.coursework.courswork.entities.Worker;
import org.springframework.data.repository.CrudRepository;

public interface WorkerRepository extends CrudRepository<Worker, Integer> {
}
