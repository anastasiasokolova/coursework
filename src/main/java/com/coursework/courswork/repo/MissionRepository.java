package com.coursework.courswork.repo;

import com.coursework.courswork.entities.Mission;
import org.springframework.data.repository.CrudRepository;

public interface MissionRepository extends CrudRepository<Mission, Integer> {
}
