package com.coursework.courswork.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class World {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String type;

    public World(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "World{" +
                "id=" + id +
                ", type='" + type + '\'' +
                '}';
    }
}
