package com.coursework.courswork.entities;

import javax.persistence.*;



@Entity
public class Demon_card {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private int worker_id;
    private int salary;

    public Demon_card(){}

    public Demon_card(int worker_id, int salary) {
        this.worker_id = worker_id;
        this.salary = salary;
    }

    public Demon_card(int salary) {
        this.salary = salary;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWorker_id() {
        return worker_id;
    }

    public void setWorker_id(int worker_id) {
        this.worker_id = worker_id;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Demon_card{" +
                "id=" + id +
                ", worker_id=" + worker_id +
                ", salary=" + salary +
                '}';
    }
}