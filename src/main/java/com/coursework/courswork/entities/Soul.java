package com.coursework.courswork.entities;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.persistence.*;

@Entity

public class Soul {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String territory;
    private String status;
    public String image_profile;

    public Soul(){

    }

    public Soul(String name, String territory, String status) {
        this.name = name;
        this.territory = territory;
        this.status = status;
    }


    public String getImage_profile() {
        return image_profile;
    }

    public void setImage_profile(String image_profile) {
        this.image_profile = image_profile;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTerritory() {
        return territory;
    }

    public void setTerritory(String territory) {
        this.territory = territory;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Soul{" +
                "id=" + id +
                ", territory='" + territory + '\'' +
                ", status='" + status + '\'' +
                ", image_profile='" + image_profile + '\'' +
                '}';
    }
}
