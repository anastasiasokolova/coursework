package com.coursework.courswork.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Mission {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private int worker_id;
    private String level;
    private String task;
    private String result;

    public Mission(){}

    //удалить столбец идб заменить на номер миссии(или просто убрать номер)
    public Mission(int worker_id, String level, String task, String result) {
        this.worker_id = worker_id;
        this.level = level;
        this.task = task;
        this.result = result;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getWorker_id() {
        return worker_id;
    }

    public void setWorker_id(int worker_id) {
        this.worker_id = worker_id;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public String
    toString() {
        return "Mission{" +
                "id=" + id +
                ", worker_id=" + worker_id +
                ", level='" + level + '\'' +
                ", task='" + task + '\'' +
                ", result='" + result + '\'' +
                '}';
    }
}
