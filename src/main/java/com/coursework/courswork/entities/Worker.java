package com.coursework.courswork.entities;

import com.coursework.courswork.repo.WorldRepository;;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Optional;

@Entity
public class Worker {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String post;
    private int world_id;
    private String duties;

    public Worker(){

    }

    public Worker(String name, String post, int world_id, String duties) {
        this.name = name;
        this.duties = duties;
        this.world_id = world_id;
        this.post = post;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public int getWorld_id() {
        return world_id;
    }

    public void setWorld_id(int world_id) {
        this.world_id = world_id;
    }

    public String getDuties() {
        return duties;
    }

    public void setDuties(String duties) {
        this.duties = duties;
    }

    @Override
    public String toString() {
        return "Worker{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", post='" + post + '\'' +
                ", world_id=" + world_id +
                ", duties='" + duties + '\'' +
                '}';
    }
}