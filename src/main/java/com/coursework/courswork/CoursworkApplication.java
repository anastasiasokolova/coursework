package com.coursework.courswork;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoursworkApplication {

    public static void main(String[] args) {
        SpringApplication.run(CoursworkApplication.class, args);
    }

}
