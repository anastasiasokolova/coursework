package com.coursework.courswork.controllers;

import com.coursework.courswork.entities.Demon_card;
import com.coursework.courswork.entities.Worker;
import com.coursework.courswork.entities.World;
import com.coursework.courswork.repo.DemonCardRepository;
import com.coursework.courswork.repo.WorkerRepository;
import com.coursework.courswork.repo.WorldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Optional;

@Controller
public class WorkerController {

    @Autowired
    private WorkerRepository workerRepository;

    @Autowired
    private WorldRepository worldRepository;

    @Autowired
    private DemonCardRepository demonCardRepository;

    @GetMapping("/worker")
    public String workerMain(Model  model) {
        Iterable<Worker> workers = workerRepository.findAll();
        model.addAttribute("workers",  workers);
        return "worker/worker-main";
    }

    @GetMapping("/worker/add")
    public String workerAdd(Model  model) {
        return "worker/worker-add";
    }

    @PostMapping("/worker/add")
    public String workerPostAdd(@RequestParam String name,
                                  @RequestParam String post,
                                  @RequestParam String duties,
                                  @RequestParam int world_id,  Model model) {
        Worker worker = new Worker(name, post, world_id, duties);
        workerRepository.save(worker);

        if (world_id == 2) {
            int workerId = worker.getId();
            Demon_card card = new Demon_card(workerId, 0);
            demonCardRepository.save(card);
        }

        return "redirect:/worker";
    }

    @GetMapping("/worker/{id}")
    public String workerInfo(@PathVariable(value = "id") int id,  Model  model) {
        Optional<Worker> worker = workerRepository.findById(id);
        ArrayList<Worker> res = new ArrayList<>();
        worker.ifPresent(res::add);
        model.addAttribute("worker", res);
        return "worker/worker-details";
    }

    @GetMapping("/worker/{id}/edit")
    public String workerEdit(@PathVariable(value = "id") int id,  Model  model) {
        Optional<Worker> worker = workerRepository.findById(id);
        ArrayList<Worker> res = new ArrayList<>();
        worker.ifPresent(res::add);
        model.addAttribute("worker", res);
        return "worker/worker-edit";
    }

    @PostMapping("/worker/{id}/edit")
    public String workerPostUpdate(@PathVariable(value = "id") int id,
                                   @RequestParam String name,
                                   @RequestParam String post,
                                   @RequestParam String duties,
                                   @RequestParam int world_id,  Model model) {
        Worker worker = workerRepository.findById(id).orElseThrow();
        worker.setDuties(duties);
        worker.setName(name);
        worker.setPost(post);
        worker.setWorld_id(world_id);
        workerRepository.save(worker);

        if (world_id == 2) {
            int workerId = worker.getId();
            Demon_card card = new Demon_card(workerId, 0);
            demonCardRepository.save(card);
        }

        return "redirect:/worker";
    }

    @PostMapping("/worker/{id}/remove")
    public String workerPostRemove(@PathVariable(value = "id") int id, Model model) {
        Worker worker = workerRepository.findById(id).orElseThrow();
        int worker_world = worker.getWorld_id();
        int workerId = worker.getId();
        workerRepository.delete(worker);

        if (worker_world == 2) {
            Demon_card card = demonCardRepository.findById(workerId).orElseThrow();
            demonCardRepository.delete(card);
        }

        return "redirect:/worker";
    }
}
