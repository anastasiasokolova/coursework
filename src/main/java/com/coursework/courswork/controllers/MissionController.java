package com.coursework.courswork.controllers;

import com.coursework.courswork.entities.Mission;
import com.coursework.courswork.repo.MissionRepository;
import org.dom4j.rule.Mode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Optional;

@Controller
public class MissionController {

    @Autowired
    private MissionRepository missionRepository;


    @GetMapping("/mission")
    public String missionMain(Model  model) {
        Iterable<Mission> missions = missionRepository.findAll();
        model.addAttribute("missions",  missions);
        return "mission/mission-main";
    }

    @GetMapping("/mission/add")
    public String missionAdd(Model  model) {
        return "mission/mission-add";
    }

    @PostMapping("/mission/add")
    public String missionPostAdd(@RequestParam int worker_id,
                              @RequestParam String level,
                              @RequestParam String task,
                              @RequestParam String result,
                              Model model) {
        Mission mission = new Mission(worker_id, level, task, result);
        missionRepository.save(mission);

        return "redirect:/mission";
    }

    @GetMapping("/mission/{id}")
    public String missionInfo(@PathVariable(value = "id") int id,  Model  model) {
        Optional<Mission> mission = missionRepository.findById(id);
        ArrayList<Mission> res = new ArrayList<>();
        mission.ifPresent(res::add);
        model.addAttribute("mission", res);
        return "mission/mission-details";
    }

    @GetMapping("/mission/{id}/edit")
    public String missionEdit(@PathVariable(value = "id") int id,  Model  model) {
        Optional<Mission> mission = missionRepository.findById(id);
        ArrayList<Mission> res = new ArrayList<>();
        mission.ifPresent(res::add);
        model.addAttribute("mission", res);
        return "mission/mission-edit";
    }

    @PostMapping("/mission/{id}/edit")
    public String missionPostUpdate(@PathVariable(value = "id") int id,
                                    @RequestParam int worker_id,
                                    @RequestParam String level,
                                    @RequestParam String task,
                                    @RequestParam String result,
                                    Model model) {
        Mission mission = missionRepository.findById(id).orElseThrow();
        mission.setWorker_id(worker_id);
        mission.setLevel(level);
        mission.setTask(task);
        mission.setResult(result);
        missionRepository.save(mission);

        return "redirect:/mission";
    }

    @GetMapping("/mission/{id}/remove")
    public String missionPostRemove(@PathVariable(value = "id") int id, Model model) {
        Mission mission = missionRepository.findById(id).orElseThrow();
        missionRepository.delete(mission);

        return "redirect:/mission";
    }

}