package com.coursework.courswork.controllers;

import com.coursework.courswork.entities.Soul;
import com.coursework.courswork.repo.SoulRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Optional;

@Controller
public class SoulController {

    @Autowired
    private SoulRepository soulRepository;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @GetMapping("/soul")
    public String soulMain(Model  model) {
        Iterable<Soul> souls = soulRepository.findAll();
        model.addAttribute("souls",  souls);
        return "soul/soul-main";
    }

    //тут пока непонятно
    @GetMapping("/soul/add")
    public String soulAdd(Model  model) {
        return "soul/soul-add";
    }

    @PostMapping("/soul/add")
    public String soulPostAdd(@RequestParam String name,
                                @RequestParam String territory,
                              //@RequestParam String image_profile,
                                Model model) {
        Soul soul = new Soul(name, territory, "Суд не пройден");
        soulRepository.save(soul);

        return "redirect:/soul";
    }

    @GetMapping("/soul/{id}")
    public String soulInfo(@PathVariable(value = "id") int id,  Model  model) {
        Optional<Soul> soul = soulRepository.findById(id);
        ArrayList<Soul> res = new ArrayList<>();
        soul.ifPresent(res::add);
        model.addAttribute("soul", res);
        return "soul/soul-details";
    }

    @GetMapping("/soul/{id}/edit")
    public String soulEdit(@PathVariable(value = "id") int id,  Model  model) {
        Optional<Soul> soul = soulRepository.findById(id);
        ArrayList<Soul> res = new ArrayList<>();
        soul.ifPresent(res::add);
        model.addAttribute("soul", res);
        return "soul/soul-edit";
    }

    @PostMapping("/soul/{id}/edit")
    public String soulPostUpdate(@PathVariable(value = "id") int id,
                                 @RequestParam String name,
                                 @RequestParam String territory,
                                 @RequestParam String image_profile,
                                 Model model) {
        Soul soul = soulRepository.findById(id).orElseThrow();
        soul.setName(name);
        soul.setImage_profile(image_profile);
        soul.setTerritory(territory);
        soulRepository.save(soul);

        return "redirect:/soul";
    }

    @PostMapping("/soul/{id}/remove")
    public String soulPostRemove(@PathVariable(value = "id") int id, Model model) {
        Soul soul = soulRepository.findById(id).orElseThrow();
        soulRepository.delete(soul);

        //по идее должны удаляться и молитвы, связанные с душойю и грехи и все такое
        //но это можно пропистаь в базе даннфх типо делит рестрикт
        return "redirect:/soul";
    }

    @GetMapping("/soul/{id}/judge")
    public String soulJudge(@PathVariable(value = "id") int id,  Model  model) {
        String sql = "SELECT court_result(?,1)";
        String ret = jdbcTemplate.queryForObject(sql, String.class, id);
        return "redirect:/soul";
    }

}
