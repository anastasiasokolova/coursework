package com.coursework.courswork.controllers;

import com.coursework.courswork.entities.Demon_card;
import com.coursework.courswork.entities.Mission;
import com.coursework.courswork.repo.DemonCardRepository;
import com.coursework.courswork.repo.MissionRepository;
import org.dom4j.rule.Mode;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.StoredProcedureQuery;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Optional;

@Controller
public class SalaryController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private DemonCardRepository salaryRepository;

    @Autowired
    private MissionRepository missionRepository;


    @GetMapping("/salary")
    public String salaryMain(Model  model) {
        Iterable<Demon_card> salary = salaryRepository.findAll();
        model.addAttribute("salary",  salary);
        return "salary/salary-main";
    }



    @GetMapping("/salary/{id}")
    public String salaryInfo(@PathVariable(value = "id") int id,  Model  model) {
        Optional<Demon_card> salary = salaryRepository.findById(id);
        ArrayList<Demon_card> res = new ArrayList<>();
        salary.ifPresent(res::add);
        model.addAttribute("salary", res);
        return "salary/salary-details";
    }

    @GetMapping("/salary/{id}/count")
    public String salaryCount(@PathVariable(value = "id") int id, Model model) {
        String sql = "SELECT demon_salary()";
        String ret = jdbcTemplate.queryForObject(sql, String.class);
        return "redirect:/salary";
    }



}